FROM registry.centos.org/centos/centos

RUN yum -y update && \
    yum -y install epel-release && \
    yum -y install stress fio && \
    yum clean all

USER user

WORKDIR /home/user

CMD ["tail", "-f", "/dev/null"]